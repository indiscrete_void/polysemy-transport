{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE EmptyDataDeriving #-}

import Control.Exception hiding (handle)
import Data.ByteString (ByteString)
import Data.ByteString qualified as B
import Data.ByteString.Char8 qualified as BC
import Data.Kind
import Data.List qualified as List
import Data.Maybe
import Data.Serialize
import Data.Void
import GHC.Generics
import Polysemy hiding (run)
import Polysemy qualified
import Polysemy.Extra.Trace
import Polysemy.Fail
import Polysemy.Serialize
import Polysemy.Transport
import Test.Tasty
import Test.Tasty.HUnit
import Transport.Maybe

type TestMessage :: Type
data TestMessage where
  Data :: String -> TestMessage
  EOF :: TestMessage
  deriving stock (Eq, Show, Generic)

instance Serialize TestMessage

type TestException :: Type
data TestException = TestException deriving stock (Show, Generic)

instance Exception TestException

assertEqual_ :: (Eq a, Show a) => a -> a -> Assertion
assertEqual_ = assertEqual ""

maybeUtils :: TestTree
maybeUtils = testGroup "Data.Maybe Utils" [testMaybeFail, testEOFToNothing, testExceptionToNothing]

dataMaybeMsg :: String
dataMaybeMsg = "this message is not important"

dataMaybeMsgB :: ByteString
dataMaybeMsgB = BC.pack dataMaybeMsg

testExceptionToNothing :: TestTree
testExceptionToNothing =
  testGroup
    "exceptionToNothing"
    [ testCase "exceptionToNothing (throw e) = pure Nothing" $ exceptionToNothing @SomeException @IO @Void (throw TestException) >>= assertEqual_ Nothing,
      testCase "exceptionToNothing (pure x) = pure (Just x)" $ exceptionToNothing @SomeException (pure dataMaybeMsg) >>= assertEqual_ (Just dataMaybeMsg)
    ]

testEOFToNothing :: TestTree
testEOFToNothing =
  testGroup
    "eofToNothing"
    [ testCase "eofToNothing empty = Nothing" $ eofToNothing B.empty @?= Nothing,
      testCase "eofToNothing str = Just str" $ eofToNothing dataMaybeMsgB @?= Just dataMaybeMsgB
    ]

testMaybeFail :: TestTree
testMaybeFail =
  testGroup
    "maybeFail"
    [ testCase "flip maybeFail Nothing = fail" $ run @Void (maybeFail dataMaybeMsg Nothing) @?= run (fail dataMaybeMsg),
      testCase "maybeFail msg . Just = pure" $
        let x = "smth" :: String
         in run (maybeFail dataMaybeMsg (pure x)) @?= pure x
    ]
  where
    run :: Sem '[Fail] a -> Maybe a
    run = runM @Maybe . failToEmbed @Maybe . raiseUnder @(Embed Maybe)

serialization :: TestTree
serialization =
  testGroup
    "Serialization"
    [ testCase "run [serialize msg] deserialize == msg" $ testSerializeDeserializeIsId (List.singleton . serializeTyped),
      testCase "run (split $ serialize msg) deserialzie == msg" $ testSerializeDeserializeIsId (split 3 . serializeTyped),
      testCase "run (serializeOutput $ output msg) == serialize msg" $
        let runTest = runM @IO . traceToStderrBuffered . fmap (listToMaybe . fst) . runOutputList
         in do
              (Just a) <- runTest (serializeOutput @TestMessage $ output serializeMsg)
              a @?= serializeTyped serializeMsg,
      testCase "run (deserializeInput $ input) == run deserialize" $
        let runTest = runM @IO . traceToStderrBuffered . failToEmbed @IO . runDecoder . runInputList [serializeTyped serializeMsg]
         in do
              (Just a) <- runTest (deserializeInput @TestMessage input)
              b <- runTest (deserializeTyped @TestMessage)
              a @?= b
    ]
  where
    serializeMsg = EOF
    testSerializeDeserializeIsId ef = do
      case Polysemy.run . runFail . runDecoder . runInputList (ef serializeMsg) $ deserializeTyped of
        Left e -> assertFailure e
        Right val -> val @?= serializeMsg
    split n str =
      if B.null str
        then []
        else
          let (part, rest) = B.splitAt n str
           in part : split n rest

runTestIO :: [i] -> Sem (Input (Maybe i) ': Output o ': r) a -> Sem r [o]
runTestIO inputList sem = fst <$> runOutputList (runInputList inputList sem)

testInputToOutput :: TestTree
testInputToOutput =
  testGroup
    "inputToOutput"
    [ testCase "Outputs all input values" $
        let inputList = ["a", "b", "c"]
         in Polysemy.run (runTestIO inputList $ inputToOutput @String) @?= inputList
    ]

testTransferStream :: TestTree
testTransferStream =
  testGroup
    "transferStream"
    [ testCase "Outputs all input values wrapped in passed f and passed message after EOF" $
        let inputList = ["a", "b", "c"]
            outputList = map Data inputList ++ [EOF]
         in Polysemy.run (runTestIO inputList $ transferStream Data EOF) @?= outputList
    ]

testHandle :: TestTree
testHandle =
  testGroup
    "handle"
    [ testCase "Calls handler function with received message" $
        let messageList = [Data "test", EOF]
         in Polysemy.runM @IO (traceToStderrBuffered (runTestIO messageList $ handle @TestMessage output)) >>= assertEqual_ messageList
    ]

polysemyFunctions :: TestTree
polysemyFunctions = testGroup "Polysemy functions" [serialization, testTransferStream, testInputToOutput, testHandle]

tests :: TestTree
tests = testGroup "Unit Tests" [maybeUtils, polysemyFunctions]

main :: IO ()
main = defaultMain tests
