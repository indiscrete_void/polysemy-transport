module Polysemy.ScopedBundle (runScopedBundle, runScopedBundle_) where

import Polysemy
import Polysemy.Bundle
import Polysemy.Internal.Sing
import Polysemy.Opaque
import Polysemy.Scoped

runScopedBundle :: (KnownList r') => (forall q. param -> InterpretersFor r' (Opaque q ': r)) -> InterpreterFor (Scoped param (Bundle r')) r
runScopedBundle f = runScopedNew \param -> f param . runBundle

runScopedBundle_ :: (KnownList r') => (forall q. InterpretersFor r' (Opaque q ': r)) -> InterpreterFor (Scoped param (Bundle r')) r
runScopedBundle_ f = runScopedBundle (const f)
