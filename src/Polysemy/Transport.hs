module Polysemy.Transport
  ( InputWithEOF,
    ByteInputWithEOF,
    ByteOutput,
    inputter,
    outputter,
    inputToIO,
    outputToIO,
    inputOrFail,
    inputJust,
    module Polysemy.Input,
    module Polysemy.Output,
    module Polysemy.Close,
    transferStream,
    handle,
    TransportEffects,
    inputToOutput,
  )
where

import Data.ByteString
import Data.Kind
import Pipes hiding (Effect, embed)
import Pipes.Prelude qualified as P
import Polysemy
import Polysemy.Close
import Polysemy.Extra.Trace
import Polysemy.Fail
import Polysemy.Input
import Polysemy.Output
import Polysemy.Trace
import System.IO
import Transport.Maybe

type TransportEffects :: Type -> Type -> [Effect]
type TransportEffects i o = '[InputWithEOF i, Output o, Close]

type InputWithEOF :: Type -> Effect
type InputWithEOF i = Input (Maybe i)

inputOrFail :: (Member (InputWithEOF a) r, Member Fail r) => Sem r a
inputOrFail = input >>= maybeFailEOF

inputJust :: (Member (InputWithEOF i) r, Member Fail r) => InterpreterFor (Input i) r
inputJust = interpret \case Input -> inputOrFail

type ByteInputWithEOF :: Effect
type ByteInputWithEOF = InputWithEOF ByteString

type ByteOutput :: Effect
type ByteOutput = Output ByteString

inputter :: (Member (InputWithEOF a) r) => Producer a (Sem r) ()
inputter = P.repeatM input >-> justYielder

outputter :: (Member (Output a) r) => Consumer a (Sem r) ()
outputter = P.mapM_ output

inputToIO :: (Member (Embed IO) r, Member Trace r) => Int -> Handle -> InterpreterFor ByteInputWithEOF r
inputToIO bufferSize h m = embed (disableBuffering h) >> (traceTagged ("inputToIO " <> show h) . go . raiseUnder @Trace $ m)
  where
    go = interpret \case
      Input -> do
        str <- embed $ eofToNothing <$> hGetSome h bufferSize
        trace $ show str
        pure str

outputToIO :: (Member (Embed IO) r, Member Trace r) => Handle -> InterpreterFor ByteOutput r
outputToIO h m = embed (disableBuffering h) >> (traceTagged ("outputToIO " <> show h) . go . raiseUnder @Trace $ m)
  where
    go = interpret \case (Output str) -> trace (show str) >> embed (hPut h str)

inputToOutput :: forall a r. (Member (InputWithEOF a) r, Member (Output a) r) => Sem r ()
inputToOutput = runEffect $ inputter @a >-> outputter

transferStream :: (Member (InputWithEOF a) r, Member (Output msg) r, Member (Output eofMsg) r) => (a -> msg) -> eofMsg -> Sem r ()
transferStream f eof = runEffect (inputter >-> P.map f >-> outputter) >> output eof

handle :: (Member (InputWithEOF msg) r, Member Trace r, Show msg) => (msg -> Sem r ()) -> Sem r ()
handle f = runEffect $ for inputter go
  where
    go msg = lift $ traceTagged "handle" (trace $ show msg) >> f msg

disableBuffering :: Handle -> IO ()
disableBuffering = (`hSetBuffering` NoBuffering)
