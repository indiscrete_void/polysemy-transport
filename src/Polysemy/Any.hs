module Polysemy.Any
  ( InputAny (..),
    InputAnyWithEOF,
    OutputAny (..),
    Any,
    inputAny,
    outputAny,
    inputToAny,
    outputToAny,
    ioToAny,
    inputToOutputAny,
    inputAnyOrFail,
  )
where

import Control.Monad.Extra
import Data.Kind
import Polysemy
import Polysemy.Fail
import Polysemy.Transport
import Transport.Maybe

type InputAny :: (Type -> Type) -> (Type -> Constraint) -> Effect
data InputAny f c m a where
  InputAny :: (c i) => InputAny f c m (f i)

type InputAnyWithEOF :: (Type -> Constraint) -> Effect
type InputAnyWithEOF = InputAny Maybe

type OutputAny :: (Type -> Constraint) -> Effect
data OutputAny c m a where
  OutputAny :: (c o) => o -> OutputAny c m ()

makeSem_ ''InputAny

inputAny :: forall i f c r. (Member (InputAny f c) r) => (c i) => Sem r (f i)

makeSem_ ''OutputAny

outputAny :: forall o c r. (Member (OutputAny c) r, c o) => o -> Sem r ()

type Any :: (Type -> Constraint) -> [Effect]
type Any c = '[InputAnyWithEOF c, OutputAny c, Close]

inputToAny :: forall i f c r. (Member (InputAny f c) r, c i) => InterpreterFor (Input (f i)) r
inputToAny = interpret \case
  Input -> inputAny @_ @f @c

outputToAny :: forall o c r. (Member (OutputAny c) r, c o) => InterpreterFor (Output o) r
outputToAny = interpret \case
  Output o -> outputAny @o @c o

ioToAny :: forall i o fi ci co r. (Member (InputAny fi ci) r, ci i, Member (OutputAny co) r, co o) => InterpretersFor '[Input (fi i), Output o] r
ioToAny = outputToAny @o @co . inputToAny @i @fi @ci

inputToOutputAny :: forall msg c r. (Member (InputAnyWithEOF c) r, Member (OutputAny c) r, c msg) => Sem r ()
inputToOutputAny = whenJustM (inputAny @msg @Maybe @c) (outputAny @_ @c)

inputAnyOrFail :: forall i c r. (Member (InputAnyWithEOF c) r, c i, Member Fail r) => Sem r i
inputAnyOrFail = inputAny @i @Maybe @c >>= maybeFailEOF
