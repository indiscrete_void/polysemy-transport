module Polysemy.Serialize
  ( Decoder,
    runDecoder,
    deserializeFrom,
    deserialize,
    serialize,
    deserializeAnyInput,
    serializeAnyOutput,
    serializeOutput,
    deserializeInput,
    deserializeTyped,
    serializeTyped,
  )
where

import Control.Constraint
import Control.Monad
import Data.ByteString (ByteString)
import Data.Dynamic
import Data.Kind
import Data.Serialize
import Data.Serialize qualified as Serial
import Data.Typeable
import GHC.Generics
import Polysemy hiding (send)
import Polysemy.Any
import Polysemy.Extra.Trace
import Polysemy.Fail
import Polysemy.Output
import Polysemy.State as State
import Polysemy.Trace
import Polysemy.Transport
import Text.Printf qualified as Text

type Typed :: Type -> Type
data Typed a = Typed
  { typedTag :: String,
    typedValue :: a
  }
  deriving stock (Show, Eq, Generic)
  deriving anyclass (Serialize)

type Decoder :: Polysemy.Effect
type Decoder = State (Maybe ByteString)

runSerial :: (Serialize a) => (Get a -> b -> f a) -> b -> f a
runSerial f = f Serial.get

runDecoder :: Sem (Decoder ': r) a -> Sem r a
runDecoder = evalState Nothing

deserializeFrom :: forall a r. (Member Decoder r, Serialize a, Member Fail r) => Sem r ByteString -> Sem r a
deserializeFrom m = takeState >>= maybe m pure >>= go . runSerial runGetPartial
  where
    takeState = State.get <* State.put @(Maybe ByteString) Nothing
    putJust = State.put . Just
    go (Serial.Fail msg left) = putJust left >> fail msg
    go (Done a left) = putJust left >> pure a
    go (Partial f) = m >>= go . f

deserialize :: (Serialize a, Member Decoder r, Member Fail r, Member ByteInputWithEOF r) => Sem r a
deserialize = deserializeFrom inputOrFail

deserializeTyped :: forall a r. (Typeable a, Serialize a, Member Decoder r, Member Fail r, Member ByteInputWithEOF r) => Sem r a
deserializeTyped = do
  (Typed tag value) <- deserialize @(Typed ByteString)
  let expectedTag = show $ typeRep (Proxy @a)
  when (expectedTag /= tag) $ fail (Text.printf "type mismatch: expected `%s`, got `%s`" expectedTag tag)
  either fail pure $ runSerial runGet value

serialize :: (Serialize a) => a -> ByteString
serialize = encode

serializeTyped :: (Serialize a, Typeable a) => a -> ByteString
serializeTyped a = serialize $ Typed (show $ typeOf a) (serialize a)

deserializeAnyInput :: (Member ByteInputWithEOF r, Member Decoder r, Member Fail r, Member Trace r) => InterpreterFor (InputAny Maybe (Show :&: Serialize :&: Typeable)) r
deserializeAnyInput = interpret \case
  InputAny -> traceTagged "deserialize" do
    i <- deserializeTyped
    trace . show $ i
    pure (Just i)

serializeAnyOutput :: (Member ByteOutput r, Member Trace r) => InterpreterFor (OutputAny (Show :&: Serialize :&: Typeable)) r
serializeAnyOutput = interpret \case
  OutputAny o -> traceTagged "serialize" do
    trace $ show o
    output $ serializeTyped o

deserializeInput :: forall a r. (Serialize a, Member Decoder r, Member Fail r, Member ByteInputWithEOF r, Member Trace r, Show a, Typeable a) => InterpreterFor (InputWithEOF a) r
deserializeInput =
  deserializeAnyInput
    . inputToAny @a @_ @(Show :&: Serialize :&: Typeable)
    . raiseUnder @(InputAny Maybe (Show :&: Serialize :&: Typeable))

serializeOutput :: forall a r. (Serialize a, Member ByteOutput r, Member Trace r, Show a, Typeable a) => InterpreterFor (Output a) r
serializeOutput = serializeAnyOutput . outputToAny @a @(Show :&: Serialize :&: Typeable) . raiseUnder @(OutputAny (Show :&: Serialize :&: Typeable))
