module Polysemy.Extra.Trace (traceToStdoutBuffered, traceToStderrBuffered, traceTagged, mapTrace) where

import Polysemy
import Polysemy.Trace
import System.IO
import Text.Printf

mapTrace :: (Member Trace r) => (String -> String) -> InterpreterFor Trace r
mapTrace f = interpret \case
  Trace msg -> trace (f msg)

traceTagged :: (Member Trace r) => String -> InterpreterFor Trace r
traceTagged = mapTrace . printf "%s: %s"

traceToStdoutBuffered :: (Member (Embed IO) r) => InterpreterFor Trace r
traceToStdoutBuffered m = embed (hSetBuffering stdout LineBuffering) >> traceToStdout m

traceToStderrBuffered :: (Member (Embed IO) r) => InterpreterFor Trace r
traceToStderrBuffered m = embed (hSetBuffering stderr LineBuffering) >> traceToStderr m
