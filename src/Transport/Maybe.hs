module Transport.Maybe
  ( maybeFail,
    eofToNothing,
    justYielder,
    whenMaybe,
    exceptionToNothing,
    ioErrorToNothing,
    maybeFailEOF,
  )
where

import Control.Exception hiding (try)
import Control.Monad.Catch
import Data.ByteString
import Pipes
import Polysemy

maybeFail :: (MonadFail m) => String -> Maybe a -> m a
maybeFail str = maybe (fail str) pure

maybeFailEOF :: (MonadFail m) => Maybe a -> m a
maybeFailEOF = maybeFail "eof reached"

eofToNothing :: ByteString -> Maybe ByteString
eofToNothing str =
  if str == empty
    then Nothing
    else Just str

exceptionToNothing :: forall e m a. (Exception e, MonadCatch m) => m a -> m (Maybe a)
exceptionToNothing m = either (const Nothing) Just <$> try @m @e m

whenMaybe :: (Applicative m) => Bool -> m (Maybe a) -> m (Maybe a)
whenMaybe True m = m
whenMaybe False _ = pure Nothing

justYielder :: Pipe (Maybe a) a (Sem r) ()
justYielder = await >>= maybe (pure ()) go
  where
    go a = yield a >> justYielder

ioErrorToNothing :: forall m a. (MonadCatch m) => m a -> m (Maybe a)
ioErrorToNothing m = either (const Nothing) Just <$> try @m @IOError m
