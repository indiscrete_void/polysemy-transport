module Control.Constraint ((:&:)) where

import Data.Kind

-- from https://hackage.haskell.org/package/exists-0.2/docs/Control-Constraint-Combine.html

type (:&:) :: (Type -> Constraint) -> (Type -> Constraint) -> (Type -> Constraint)
class (c a, d a) => (c :&: d) a

instance (c a, d a) => (c :&: d) a

infixr 7 :&:
